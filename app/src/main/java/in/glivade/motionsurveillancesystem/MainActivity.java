package in.glivade.motionsurveillancesystem;

import static in.glivade.motionsurveillancesystem.app.Constant.MODE_AUTO;
import static in.glivade.motionsurveillancesystem.app.Constant.MODE_MANUAL;
import static in.glivade.motionsurveillancesystem.app.Constant.URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.glivade.motionsurveillancesystem.app.AppController;
import in.glivade.motionsurveillancesystem.app.MyPreference;
import in.glivade.motionsurveillancesystem.app.ToastBuilder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int UPDATE_DELAY = 10000;
    private Context mContext;
    private ImageView mImageViewLeft, mImageViewStream, mImageViewRight;
    private Button mButtonRegisterDevice, mButtonMode;
    private Handler mHandler;
    private Runnable mRunnable;
    private MyPreference mPreference;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        initUpdateTimer();
        getImage();
        invalidateRegisterDevice();
        invalidateMode();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageViewLeft) {
            int position = mPreference.getPosition();
            if (position > 0) {
                mPreference.setPosition(position - 1);
                updatePosition();
            }
        } else if (v == mImageViewRight) {
            int position = mPreference.getPosition();
            if (position < 10) {
                mPreference.setPosition(position + 1);
                updatePosition();
            }
        } else if (v == mButtonMode) {
            mPreference.setMode(mPreference.getMode().equals(MODE_AUTO) ? MODE_MANUAL : MODE_AUTO);
            invalidateMode();
            updateMode();
        } else if (v == mButtonRegisterDevice) {
            mPreference.setTokenUploaded(true);
            invalidateRegisterDevice();
            launchEmail();
        }
    }

    private void initObjects() {
        mImageViewLeft = (ImageView) findViewById(R.id.img_left);
        mImageViewStream = (ImageView) findViewById(R.id.img_stream);
        mImageViewRight = (ImageView) findViewById(R.id.img_right);
        mButtonRegisterDevice = (Button) findViewById(R.id.btn_register_device);
        mButtonMode = (Button) findViewById(R.id.btn_mode);

        mContext = this;
        mHandler = new Handler();
        mPreference = new MyPreference(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mImageViewLeft.setOnClickListener(this);
        mImageViewRight.setOnClickListener(this);
        mButtonRegisterDevice.setOnClickListener(this);
        mButtonMode.setOnClickListener(this);
    }

    private void initUpdateTimer() {
        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPreference.getMode().equals(MODE_AUTO)) getImage();
                mHandler.postDelayed(mRunnable, UPDATE_DELAY);
            }
        };
        mRunnable.run();
    }

    private void getImage() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleImageResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ToastBuilder.build(mContext, error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "image");
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "image");
    }

    private void handleImageResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            String image = jsonObject.getString("image");
            Glide.with(mContext).load(image).diskCacheStrategy(
                    DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(
                    mImageViewStream);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updatePosition() {
        showProgressDialog("Updating position..");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            ToastBuilder.build(mContext, message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                ToastBuilder.build(mContext, error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "position");
                params.put("position", String.valueOf(mPreference.getPosition()));
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "position");
    }

    private void updateMode() {
        showProgressDialog("Updating mode..");
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String message = jsonObject.getString("message");
                            ToastBuilder.build(mContext, message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                ToastBuilder.build(mContext, error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("tag", "mode");
                params.put("mode", mPreference.getMode());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "mode");
    }

    private void invalidateMode() {
        String mode = mPreference.getMode();
        mButtonMode.setText(mode);
    }

    private void invalidateRegisterDevice() {
        if (mPreference.getFcmToken() == null || mPreference.isTokenUploaded()) {
            mButtonRegisterDevice.setVisibility(View.GONE);
        } else {
            mButtonRegisterDevice.setVisibility(View.VISIBLE);
        }
    }

    private void launchEmail() {
        Uri data = Uri.parse("mailto:?body=" + mPreference.getFcmToken());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(data);
        startActivity(intent);
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
