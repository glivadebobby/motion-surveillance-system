package in.glivade.motionsurveillancesystem.app;

/**
 * Created by Bobby on 20-04-2017
 */

public class Constant {
    public static final String MODE_AUTO = "auto";
    public static final String MODE_MANUAL = "manual";
    public static final String URL = "http://motiondetection.glivade.in";
}
