package in.glivade.motionsurveillancesystem.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

import in.glivade.motionsurveillancesystem.MainActivity;
import in.glivade.motionsurveillancesystem.R;

/**
 * Created by Bobby on 20-04-2017
 */

public class FirebaseNotification extends FirebaseMessagingService {

    private static final int REQUEST_NOTIFICATION = 4;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        String header = null;
        String body = null;
        try {
            header = jsonObject.getString("header");
            body = jsonObject.getString("body");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        displayNotification(header, body);
    }

    private void displayNotification(String header, String body) {
        PendingIntent pendingIntentContent = PendingIntent.getActivity(this, REQUEST_NOTIFICATION,
                new Intent(this, MainActivity.class), PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setTicker(header)
                .setContentTitle(header)
                .setContentText(body)
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.ic_notify)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setDefaults(Notification.DEFAULT_ALL);

        Random random = new Random();
        int notificationId = random.nextInt(9999 - 1000) + 1000;

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, builder.build());
    }
}
