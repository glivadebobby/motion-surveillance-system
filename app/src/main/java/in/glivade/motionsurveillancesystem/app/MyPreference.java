package in.glivade.motionsurveillancesystem.app;

import static in.glivade.motionsurveillancesystem.app.Constant.MODE_AUTO;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by Bobby on 20-04-2017
 */

public class MyPreference {

    private static final String PREF_EXTRAS = "extras";
    private static final String IMAGE = "image";
    private static final String POSITION = "position";
    private static final String MODE = "mode";
    private static final String FCM_TOKEN = "fcm_token";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private SharedPreferences mPreferencesExtras;

    public MyPreference(Context context) {
        mPreferencesExtras = context.getSharedPreferences(PREF_EXTRAS, Context.MODE_PRIVATE);
    }

    public String getImage() {
        return mPreferencesExtras.getString(encode(IMAGE), null);
    }

    public void setImage(String image) {
        mPreferencesExtras.edit().putString(encode(IMAGE), image).apply();
    }

    public int getPosition() {
        return mPreferencesExtras.getInt(encode(POSITION), 5);
    }

    public void setPosition(int position) {
        mPreferencesExtras.edit().putInt(encode(POSITION), position).apply();
    }

    public String getMode() {
        return mPreferencesExtras.getString(encode(MODE), MODE_AUTO);
    }

    public void setMode(String command) {
        mPreferencesExtras.edit().putString(encode(MODE), command).apply();
    }

    public boolean isTokenUploaded() {
        return mPreferencesExtras.getBoolean(encode(TOKEN_UPLOADED), false);
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        mPreferencesExtras.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public String getFcmToken() {
        return mPreferencesExtras.getString(encode(FCM_TOKEN), null);
    }

    void setFcmToken(String fcmToken) {
        mPreferencesExtras.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
