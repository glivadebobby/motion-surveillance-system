package in.glivade.motionsurveillancesystem.app;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Bobby on 20-04-2017
 */

public class ToastBuilder {
    public static void build(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
